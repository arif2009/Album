# Simple Album Project Using React Native

(https://www.versioneye.com/user/projects/561946aca193340f320004b1)
[![License](https://img.shields.io/npm/l/express.svg)](http://opensource.org/licenses/MIT)

# Technologies

* [React](https://reactjs.org/)
* [React Native 0.50.4](https://facebook.github.io/react-native/)
* ["axios 0.17.1" for Promise based HTTP client](https://github.com/axios/axios)

# Installation

Cloning the repository:

```bash
# HTTPS clone URL
$ git clone https://github.com/arif2009/Album.git

# SSH clone URL
$ git clone git@github.com:arif2009/Album.git
```

You need to have [Git](https://git-scm.com/), [Node.js](https://nodejs.org/en/) and [Python 2 (Not mandatory)](https://www.python.org/) installed on your machine before running the followings:

```bash

$ cd /path/to/Album
$ npm install

# Build and run the solution
```
For more information please visit [bower.io](http://bower.io/) and [gulp.js](http://gulpjs.com/)

# License

This application is released under the [MIT](http://www.opensource.org/licenses/MIT) License.

Copyright (c) 2017 [Arifur Rahman (Sazal)](http://arifur-rahman-sazal.blogspot.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.